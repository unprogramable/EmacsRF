;; setup-package.el
;; Package configs
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("melpa"        . "https://melpa.org/packages/")
;;                       ("melpa-stable" . "http://stable.melpa.org/packages/")
;;                       ("gnu"   . "http://elpa.gnu.org/packages/")
;;                       ("org"   . "http://orgmode.org/elpa/")
                         ))
(package-initialize)

;; Require Packages
(defvar my-packages
  '(auto-complete      ;; auto Completion for GNU Emacs
    diff-hl            ;; highlight uncommitted changes using Version Control
    neotree            ;; a tree plugin like NerdTree for Vim ( required mode-icons )
    mode-icons         ;; Show icons for mode
    powerline          ;; rewrite of Powerline
    ;;---web package
    web-mode           ;; highlighting, indentation, closing tags, jumping tags, commenting
    company-web        ;; completion of keywords as you type
    yasnippet          ;; shortcut to write a snippet
    emmet-mode         ;; smarter yasnippet
    ))


;; check and load packages (local)
;; packages you manually installed
(setq site-lisp
      (expand-file-name "site-lisp" user-emacs-directory))

;;(if (file-directory-p site-lisp)
(add-to-list 'load-path site-lisp)

;; load each project inside site-lisp directory
(dolist (project (directory-files site-lisp t "\\w+"))
  (when (file-directory-p project)
    (add-to-list 'load-path project)))

;; check and install require package (net)
;; if list packges is not loaded fetch the list of packages available
(when (not package-archive-contents)
  (package-refresh-contents))

;; install extensions if they're missing or not installed
(dolist (package-name my-packages)
  (when (not (package-installed-p package-name))
    (package-install package-name)))

;;;; enable and config require package
;;====================================

;; Auto Completion for GNU Emacs
(require 'auto-complete)
(global-auto-complete-mode t)

;; Highlight uncommitted changes using VC
(require 'diff-hl)
(global-diff-hl-mode)

;; a tree plugin like NerdTree for Vim
(require 'neotree)
(setq neo-theme 'fileicons)
(global-set-key [f5] 'neotree-toggle)

;; icons for emacs
(require 'mode-icons)
(mode-icons-mode)

;; Rewrite of Powerline. better power line
(require 'powerline)
(powerline-default-theme)

;;;; enable and config other package
;;====================================
;; web mode setup ; setting > setup-web-mode.el
(require 'setup-web-mode)


;; provide setup-package
(provide 'setup-package)

;;; setup-package.el ends here
