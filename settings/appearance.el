;; something about faster startup
(modify-frame-parameters nil '((wait-for-wm . nil)))

;; Disable menubar, toolbar, tooltip, scrollbar
(if (fboundp 'scroll-bar-mode)(scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode  )(tool-bar-mode   -1))
(if (fboundp 'tooltip-mode   )(tooltip-mode    -1))
(if (fboundp 'menu-bar-mode  )(menu-bar-mode   -1))
(if (fboundp 'horizontal-scroll-bar-mode)(horizontal-scroll-bar-mode -1))

;; no startup-message
(setq inhibit-startup-message -1)

;; display line number in all buffers (i dont use line number now)
;(when (version<= "26.0.50" emacs-version )
;  (global-display-line-numbers-mode))

;; global Highlight current line true
(global-hl-line-mode t)

;; Highlight matching parentheses when the point is on them.
(show-paren-mode t)
(setq-default show-paren-delay 0)

;; Emcas title
(when window-system
  (setq frame-title-format '(buffer-file-name "( %b )")))

;; load theme path themes
(setq custom-theme-directory (concat user-emacs-directory "themes"))
(dolist (path (directory-files custom-theme-directory t "\\w+"))
  (when (file-directory-p path)
    (add-to-list 'custom-theme-load-path path)))

;; mini theme
(load-theme 'minimax t)

;; provide appearance
(provide 'appearance)

;;; appearance.el ends here
