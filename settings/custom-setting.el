;;;; Editor settigs
;;==============================
;; initial mode
(setq initial-major-mode 'text-mode)

;; Allow pasting selection outside of Emacs
(setq x-select-enable-clipboard t)

;; Make searches case insensitive
(setq case-fold-search t)

;; Show keystrokes in progress
(setq echo-keystrokes 0.1)

;; slower scrolling up/down
(setq mouse-wheel-scroll-amount '(2 ((shift) . 0)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq mouse-wheel-follow-mouse 't)
(setq scroll-conservatively 50)
(setq scroll-up-margin 5)
(setq scroll-margin 5)
(setq scroll-step 1)

;; don't blink cursor
(blink-cursor-mode 0)

;; don't show these buffers when trying to switch
(setq buffer-stack-untracked '("*Completions*" "*scratch*" "*Messages*" "*Backtrace*" "*Warnings*"))

;; No electric indent
(setq electric-indent-mode t)

;; auto close bracket insertion
(electric-pair-mode -1)

;; Make all "yes or no" prompts show "y or n" instead
(fset 'yes-or-no-p 'y-or-n-p)

;; replace highlighted text with type or inserting text
(delete-selection-mode 1)

;; save cursor position between sessions
(save-place-mode)

;; Auto refresh buffers
(global-auto-revert-mode 1)


;; 80 chars is a good width.
(set-default 'fill-column 80)

;; Never insert tabs
(setq-default indent-tabs-mode nil)
(setq-default indent-line-function nil)
(setq-default tab-width 4)



;; Don't break lines
(setq-default truncate-lines t)

;; Real emacs users don't use shift to mark things 'use C+SPE or C+x SPE'
(setq-default shift-select-mode nil)

;; Don't highlight matches with jump-char - it's distracting
(setq jump-char-lazy-highlight-face nil)

;; Add parts of each file's directory to the buffer name if not unique
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; A saner ediff
(setq ediff-diff-options "-w")
(setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;;;;dired
;;==============================
;; Also auto refresh dired, but be quiet about it
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)


;;;; file settings
;;==============================
;; UTF-8
(setq locale-coding-system   'utf-8)
(set-terminal-coding-system  'utf-8)
(set-keyboard-coding-system  'utf-8)
(set-language-environment    'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system        'utf-8)
(set-locale-environment "en_US.UTF-8")
(setenv "LANG"              "en_US.UTF-8")
(setenv "LC_ALL"            "en_US.UTF-8")
(setenv "LC_CTYPE"          "en_US.UTF-8")
(setenv "LC_NUMERIC"        "en_US.UTF-8")
(setenv "LC_TIME"           "en_US.UTF-8")
(setenv "LC_COLLATE"        "en_US.UTF-8")
(setenv "LC_MONETARY"       "en_US.UTF-8")
(setenv "LC_PAPER"          "en_US.UTF-8")
(setenv "LC_NAME"           "en_US.UTF-8")
(setenv "LC_ADDRESS"        "en_US.UTF-8")
(setenv "LC_TELEPHONE"      "en_US.UTF-8")
(setenv "LC_MEASUREMENT"    "en_US.UTF-8")
(setenv "LC_IDENTIFICATION" "en_US.UTF-8")

;; Make unix unicode default
(setq-default default-buffer-file-coding-system 'utf-8-unix)

;; Don't write lock-files
(setq create-lockfiles nil)

;; Disable auto save and backup files
; (setq make-backup-files nil) ; stop creating backup
(setq auto-save-default nil) ; stop creating #autosave# files

;; Move files to trash when deleting
(setq delete-by-moving-to-trash t)

;; Save minibuffer history
(savehist-mode 1)
(setq history-length 100)

;; delete trailing whitespace when file saved (Emacs 21 and later)
(add-hook 'before-save-hook
          'delete-trailing-whitespace)

;; Offer to create parent directories if they do not exist
(defun create-non-existent-directory ()
  (let ((parent-directory (file-name-directory buffer-file-name)))
    (when (and (not (file-exists-p parent-directory))
               (y-or-n-p (format "Directory `%s' does not exist! Create it?" parent-directory)))
      (make-directory parent-directory t))))

(add-to-list 'find-file-not-found-functions 'create-non-existent-directory)


;; Write backup files to own directory
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

;; provide custom-setting
(provide 'custom-setting)

;;; custom-setting.el ends here
