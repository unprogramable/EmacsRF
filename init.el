;; init.el
;; Author  : unprogramable
;; Requires: ((emacs25 or newer))

;;; Code:

;; Uncomment this for debugging purposes
;;(setq debug-on-error t)

;;;; settings directory
;;==============================
(setq settings-dir
      (expand-file-name "settings" user-emacs-directory))

(add-to-list 'load-path settings-dir)

;; Load From settings -> appearance.el
(require 'appearance)

;; Load From settings -> custom-setting.el
(require 'custom-setting)

;; check/install and load/config require packages
(require 'setup-package)

;; add my Functions (load all files in defuns-dir)
(setq defuns-dir
      (expand-file-name "defuns" user-emacs-directory))

;; load each file in defun folder
(dolist (file (directory-files defuns-dir t "\\w+"))
  (when (file-regular-p file)
    (load file)))

;; Load From settings -> key-bindings
(require 'key-bindings)
